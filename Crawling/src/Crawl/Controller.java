/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Crawl;

import DataManager.DbManager;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

import java.util.Timer;

public class Controller {
    static final String usrAgent = "UCI Inf141-CS121 crawler 801225555 74151797";
    static final int politenessDelay = 500;
    public static Timer t = new Timer();

    int crawlers = 1;
    CrawlConfig config;
    CrawlController ctrl;

    public static void main(String[] args)
            throws Exception {
        DbManager.promptCredentialsByConsole();
        Controller control = new Controller(10);
        control.begin();
    }

    public Controller(int n)
            throws Exception {
        crawlers = n;
        config = initConfig();
        ctrl = initController(config);
        ctrl.addSeed("http://www.ics.uci.edu");
    }

    public Controller()
            throws Exception {
        config = initConfig();
        ctrl = initController(config);
        ctrl.addSeed("http://www.ics.uci.edu");
    }

    private static CrawlController initController(CrawlConfig config)
            throws Exception {
        PageFetcher pgFetch = new PageFetcher(config);
        RobotstxtConfig botTxtCfg = new RobotstxtConfig();
        RobotstxtServer botTxtServ = new RobotstxtServer(botTxtCfg, pgFetch);
        return new CrawlController(config, pgFetch, botTxtServ);
    }

    private static CrawlConfig initConfig() {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder("res/crawling/root");
        config.setMaxDepthOfCrawling(-1);
        config.setPolitenessDelay(politenessDelay);
        config.setUserAgentString(usrAgent);
        config.setResumableCrawling(true);
        return config;
    }

    public void begin() {
        ctrl.start(Crawler.class, crawlers);
    }

    public void stop() {
        ctrl.shutdown();
    }



}
