/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Crawl;

import edu.uci.ics.crawler4j.url.WebURL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

public class WebSite implements Serializable {
    private String url;
    private String contentText;
    private String contentHtml;
    private ArrayList<WebURL> outgoingLinks = null;


    public WebSite(String url, String contentText, String contentHtml,
                   Set<WebURL> outgoing) {
        this.url = url;
        this.contentText = contentText;
        this.contentHtml = contentHtml;
        this.outgoingLinks = new ArrayList<>(outgoing);
    }

    public WebSite() {
    }

    public WebSite(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o){
        return o instanceof String
                && o.equals(url);
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public String getContentHtml() {
        return contentHtml;
    }

    public void setContentHtml(String contentHtml) {
        this.contentHtml = contentHtml;
    }

    @Override
    public String toString(){
        return url;
    }

    public ArrayList<WebURL> getOutgoingLinks() {
        return outgoingLinks;
    }

    public void setOutgoingLinks(ArrayList<WebURL> outgoingLinks) {
        this.outgoingLinks = outgoingLinks;
    }

    public String getInlineOutgoingLinks() {
        String inline = "";
        for (int i = 0; i < outgoingLinks.size(); ++i) {
            inline += outgoingLinks.get(i).getURL();
            if (i != outgoingLinks.size() - 1)
                inline += "[;;]";
        }
        return inline;
    }

}
