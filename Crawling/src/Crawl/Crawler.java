/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Crawl;

import DataManager.DbManager;
import StrUtil.GenStr;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Set;
import java.util.regex.Pattern;

public class Crawler extends WebCrawler {
    public final static String[] doNotCrawl = new String[]{
            "calendar.ics.uci.edu/calendar.php",
            "calendar.ics.uci.edu"
    };
    private static final Logger logger = LoggerFactory.getLogger(Crawler.class);
    private static final Pattern FILTERS = Pattern.compile(
            ".*(\\.(css|js|bmp|gif|jpe?g"
                    + "|ppt|pptx|ps|java|cs|c|cc|h|exe|zip|tar|dat|jar|txt|csv|R"
                    + "|tgz|bin|doc|docx|gz|mat|mso|iso|js|xml|json|thmx|data|lisp|lua"
                    + "|png|tiff?|mid|mp2|mp3|mp4"
                    + "|wav|avi|mov|mpe?g|ram|m4v|pdf"
                    + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
    int websites = 0;

    private int blocked, filtered, notInDom, visited;

    public Crawler() {
        loadFromDb();
    }

    public static boolean isBlockedUrl(String s) {
        String stripped = GenStr.stripHttp(s);
        for (String itm : doNotCrawl)
            if (itm.equals(stripped)
                    || stripped.contains(itm))
                return true;
        return false;
    }

    public static boolean isInDom(String url) {
        return (url.contains(".") &&
                url.contains(".ics.uci.edu") &&
                url.indexOf(".ics.uci.edu") - url.indexOf(".") == 0)
                || (url.indexOf(".") == url.indexOf("ics.uci.edu") + 3);
    }


    private void loadFromDb() {
        try {
            int ret = DbManager.getInstance().initWebTable().executeUpdate();
            websites = DbManager.getInstance().getWebSiteCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        String rejectReason = "";
        boolean allowed = true;

        if (!isInDom(href)) {
            rejectReason = "Not in main domain.";
            notInDom++;
            allowed = false;
        } else if (isBlockedUrl(href)) {
            rejectReason = "Blocked site.";
            blocked++;
            allowed = false;
        } else if (FILTERS.matcher(GenStr.getBeforeUrlQuery(href)).matches()) {
            rejectReason = "Matches filter pattern.";
            filtered++;
            allowed = false;
        }
/*        else if (checkVisited(href)) {
            rejectReason = "This site has been visited.";
            visited++;
            allowed = false;
        }*/

        if (!allowed)
            logger.info(String.format("[Rejected] %s {%s}", rejectReason, href));

        return allowed;
    }

    public boolean checkVisited(String href) {
        boolean visited = false;
        try {
            visited = DbManager.getInstance().siteExists(GenStr.stripHttp(href));
        } catch (SQLException e) {
            e.printStackTrace();
        }

/*        if (!visited)
            logger.info(String.format("Not visited '%s' yet.", href));*/
        return visited;
    }

    @Override
    public void visit(Page page){
        String url = page.getWebURL().getURL();
        logger.info("Visiting " + url);

            if (page.getParseData() instanceof HtmlParseData) {
                HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
                String text = htmlParseData.getText();
                String html = htmlParseData.getHtml();
                Set<WebURL> links = htmlParseData.getOutgoingUrls();
                logger.info(String.format("Outgoing Links for '%s': %s", url, links.size()));
                WebSite ws = new WebSite(GenStr.stripHttp(url), text, html, links);
                persist(url, ws);
            }
    }

    private void persist(String url, WebSite ws) {
        try {
            if(DbManager.getInstance().websiteInsert(ws))
                websites++;
            else
                logger.info(String.format("[Bad Crawler] '%s' is a redundant visit, not persisting.", url));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info(String.format("[%d] '%s' has been persisted.",
                websites, url));
    }

    public int getRejected() {
        return filtered + blocked + notInDom;
    }

    public int getBlocked() {
        return blocked;
    }

    public int getFiltered() {
        return filtered;
    }

    public int getVisited() {
        return visited;
    }

    public int getWebsites() {
        return websites;
    }
}
