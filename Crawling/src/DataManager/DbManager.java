/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package DataManager;

import Crawl.WebSite;

import java.sql.*;
import java.util.Scanner;

public class DbManager {
    private String dbName = "beesse";
    private Connection conn = null;
    private DbCredentials creds = null;
    private PreparedStatement
            stmt = null,
            websiteInsert = null,
            websiteFetchAllUrl = null,
            websiteFetchAllUrlAndNormalized_Visits = null,
            URLContent=null,
            insertM = null,
            getDocPaging = null,
            isDictSubStr = null,
            siteExist = null,
            websiteCount = null;

    static DbManager db = null;

    public static DbManager getInstance() {
        return db;
    }

    public DbManager(DbCredentials creds) {
        this.creds = creds;
        this.db = this;
    }

    public synchronized Connection getConn()
            throws ClassNotFoundException, SQLException {
        if(conn == null) {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(
                    creds.getHost(), creds.getUsername(),
                    creds.getPassword());
        }
        return conn;
    }

    public synchronized boolean siteExists(String url) throws SQLException {
        if (siteExist == null)
            siteExist = conn.prepareStatement(
                    "SELECT count(*) FROM website WHERE url = ?;");
        siteExist.setString(1, url);
        ResultSet rs = siteExist.executeQuery();

        if(rs.next()) {
            int f = rs.getInt(1);
            return f > 0;
        }
        return false;
    }

    public synchronized boolean websiteInsert(WebSite web)
            throws SQLException, ClassNotFoundException {
        if(siteExists(web.getUrl()))
            return false;

        if(websiteInsert == null)
            websiteInsert = getConn().prepareStatement(
                    "INSERT INTO `" + dbName + "`.Website (" +
                    "Url, Html_Content, " +
                    "Text_Content, Outgoing_Links) " +
                    "VALUES (?, ?, ?, ?);");

        websiteInsert.setString(1, web.getUrl());
        websiteInsert.setString(2, web.getContentHtml());
        websiteInsert.setString(3, web.getContentText());
        websiteInsert.setString(4, web.getInlineOutgoingLinks());
        websiteInsert.executeUpdate();
        return true;
    }

    public synchronized PreparedStatement getPrepStmt_AllUrl()
            throws SQLException, ClassNotFoundException {
        if(websiteFetchAllUrl == null)
            websiteFetchAllUrl = getConn().prepareStatement(
                    "SELECT Url, Id FROM `" + dbName + "`.Website");
        return websiteFetchAllUrl;
    }

    public synchronized PreparedStatement getPrepStmt_AllUrlAndNormalized_Vists()
            throws SQLException, ClassNotFoundException {
        if(websiteFetchAllUrlAndNormalized_Visits == null)
            websiteFetchAllUrlAndNormalized_Visits = getConn().prepareStatement(
                    "SELECT Url, Noramlized_Url " +
                            "FROM `beesse`.Website " +
                            "LIMIT 10000000");
        return websiteFetchAllUrlAndNormalized_Visits;
    }

    public synchronized PreparedStatement getURLContent() throws SQLException, ClassNotFoundException {
        if(URLContent==null){
            URLContent = getConn().prepareStatement(
                    "select Url, Text_Content " +
                            "from `" + dbName + "`.Website " +
                            "limit 10000000");
        }
        return URLContent;
    }

    public synchronized PreparedStatement getTextContent(int id)
            throws SQLException, ClassNotFoundException {
        if (stmt == null)
            stmt = conn.prepareStatement(
                    "SELECT Id, Text_Content,Url " +
                            "FROM Website WHERE id = ?");
        stmt.setInt(1, id);
        return stmt;
    }

    public synchronized PreparedStatement getHtmlContent(int id)
            throws SQLException, ClassNotFoundException {
        if (stmt == null)
            stmt = conn.prepareStatement(
                    "SELECT Id, Html_Content, Url " +
                            "FROM Website WHERE id = ?");
        stmt.setInt(1, id);
        return stmt;
    }

    public synchronized PreparedStatement getTextContent()
            throws SQLException, ClassNotFoundException {
        if (stmt == null) {
            stmt = conn.prepareStatement(
                    "SELECT Id, Url, Text_Content " +
                            "FROM `" + dbName + "`.Website " +
                            "LIMIT 10000000");
        }
        return stmt;
    }

    public synchronized PreparedStatement getTextContent(int offset, int limit)
            throws SQLException, ClassNotFoundException {
        getDocPaging = conn.prepareStatement(
                "SELECT Id, Url, Text_Content " +
                        "FROM `" + dbName + "`.Website " +
                        "OFFSET " + offset +
                        " LIMIT " + limit + ";");
        return getDocPaging;
    }

    public synchronized boolean isSubstrInDictionary(String inp)
            throws SQLException, ClassNotFoundException {
        if (isDictSubStr == null)
            isDictSubStr = getConn().prepareStatement(
                    "select * from entries " +
                            "where lower(word) = \"" +
                            inp.toLowerCase() + "\";");
        ResultSet rs = isDictSubStr.executeQuery();
        return rs.next();
    }

    public synchronized String wordBySubstrInDictionary(String inp)
            throws SQLException, ClassNotFoundException {
        if (isDictSubStr == null)
            isDictSubStr = getConn().prepareStatement(
                    "select * from entries " +
                            "where lower(word) like \"%" +
                            inp.toLowerCase() + "%\";");
        ResultSet rs = isDictSubStr.executeQuery();
        if (rs.next()) return rs.getString(1);
        return null;
    }

    public synchronized void InsertIntoM(int CId, int termid, double weight) throws SQLException {
        if (insertM == null) {
            try {
                insertM = getConn().prepareStatement("INSERT INTO M VALUES (?,?,?);");
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        insertM.setInt(1, CId);
        insertM.setInt(2, termid);
        insertM.setDouble(3, weight);
        insertM.executeUpdate();
    }

    public synchronized PreparedStatement initWebTable()
            throws SQLException, ClassNotFoundException {
        return getConn().prepareStatement(
                "CREATE TABLE IF NOT EXISTS `beesse`.`website` (" +
                        "  `Id` INT NOT NULL AUTO_INCREMENT," +
                        "  `Url` VARCHAR(2083) NULL," +
                        "  `Html_Content` LONGTEXT NULL," +
                        "  `Text_Content` MEDIUMTEXT NULL," +
                        "  `Outgoing_Links` MEDIUMTEXT NULL," +
                        "  PRIMARY KEY (`id`));");
    }

    public static DbManager promptCredentialsByConsole()
            throws ClassNotFoundException {
        String dbUsername, dbPassword;
        boolean loggedIn = false;
        do {
            try {
                System.out.print("Username: ");
                Scanner inp = new Scanner(System.in);
                dbUsername = inp.next();
                System.out.print("Password: ");
                inp = new Scanner(System.in);
                dbPassword = inp.next();

                DbCredentials creds = new DbCredentials(dbUsername, dbPassword);
                DbManager mgr = new DbManager(creds);
                System.out.printf("Logging into Database '%s'...", creds.getHost());
                mgr.getConn();
                System.out.println("Success");
                loggedIn = true;
                return mgr;
            } catch (SQLException sqlEx) {
                sqlEx.getErrorCode();
                System.out.printf("[%d]: %s",
                        sqlEx.getErrorCode(),
                        sqlEx.getMessage());
                System.out.flush();
            }
        } while (!loggedIn);
        return null;
    }


    public synchronized int getWebSiteCount() throws SQLException {
        if (websiteCount == null)
            websiteCount = conn.prepareStatement("SELECT count(*) FROM website;");
        ResultSet rs = websiteCount.executeQuery();
        rs.next();
        return rs.getInt(1);
    }
}
