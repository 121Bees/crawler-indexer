/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package DataManager.Indexing;

import Indexer.IndexerUtil.TfIdfAllTerms;

import java.sql.Connection;
import java.sql.SQLException;

public class TablesBuilder {
    Connection conn;
    String name = "";
    TablesQuery query;
    TfIdfAllTerms allTerms;
    String dbName = "beesse";

    public TablesBuilder(String name, TfIdfAllTerms allTerms, Connection conn) {
        this.name = name;
        this.conn = conn;
        this.allTerms = allTerms;
        query = new TablesQuery(name, conn);
    }

    public TablesQuery getQuery() {
        return query;
    }

    public String getName() {
        return name;
    }

    public void constructIndexTables() throws SQLException {
        constructDb();
        constructParsedDocTable();
        constructIndexVectorTable();
        constructTfIdfTable();
        constructDocFreTable();

    }

    public void dropIndexTables() throws SQLException {
        conn.prepareStatement(
                "DROP TABLE IF EXISTS `" + dbName + "`.DocFreq_" + name + ", " +
                        "`" + dbName + "`.ParsedDoc_" + name + ", " +
                        "`" + dbName + "`.IndexVector_" + name + ", " +
                        "`" + dbName + "`.TfIdf_" + name + ";")
                .executeUpdate();
    }

    private void constructDb() throws SQLException {
        conn.prepareStatement("create database if not exists `" + dbName + "`;").executeUpdate();
    }

    private void constructDocFreTable() throws SQLException {
        conn.prepareStatement(
                "CREATE TABLE if not exists `" + dbName + "`.DocFreq_" +
                        name +
                        "( id INT NOT NULL AUTO_INCREMENT," +
                        "  termid INT NOT NULL UNIQUE," +
                        "  doc_freq INT NOT NULL," +
                        "  PRIMARY KEY (id)," +
                        "  INDEX fk_termid_idx_" + name + " (termid ASC)," +
                        "  CONSTRAINT fk_termid_docfreq_" + name +
                        "    FOREIGN KEY (termid)" +
                        "    REFERENCES `" + dbName + "`.Terms_" + allTerms.getName() + " (id)" +
                        "    ON DELETE NO ACTION" +
                        "    ON UPDATE NO ACTION);")
                .executeUpdate();
    }

    private void constructIndexVectorTable() throws SQLException {
        conn.prepareStatement(
                "create table if not exists IndexVector_" + name +
                        "(termid Integer, docid Integer, freq Integer);")
                .executeUpdate();
    }

    private void constructParsedDocTable() throws SQLException {
        conn.prepareStatement(
                "CREATE TABLE if not exists `" + dbName + "`.ParsedDoc_" +
                        name +
                        "( id INT NOT NULL AUTO_INCREMENT," +
                        "  doc_id INT NOT NULL UNIQUE," +
                        "  PRIMARY KEY (id, doc_id));")
                .executeUpdate();
    }

    private void constructTfIdfTable() throws SQLException {
        conn.prepareStatement(
                "CREATE TABLE if not exists `" + dbName + "`.TfIdf_" +
                        name +
                        "(    id INT NOT NULL AUTO_INCREMENT," +
                        "    termid INT NOT NULL," +
                        "    docid INT NOT NULL," +
                        "    score DOUBLE NOT NULL," +
                        "    PRIMARY KEY (id)," +
                        "    INDEX fk_termid_idx_" + name + " (termid ASC)," +
                        "    INDEX fk_docid_idx_" + name + " (docid ASC)," +
                        "    CONSTRAINT fk_termid_" + name +
                        "    FOREIGN KEY (termid)" +
                        "    REFERENCES `" + dbName + "`.Terms_" + allTerms.getName() + " (id)" +
                        "    ON DELETE NO ACTION" +
                        "    ON UPDATE NO ACTION," +
                        "    CONSTRAINT fk_docid_" + name +
                        "    FOREIGN KEY (docid)" +
                        "    REFERENCES `" + dbName + "`.Website (Id)" +
                        "    ON DELETE NO ACTION" +
                        "    ON UPDATE NO ACTION);")
                .executeUpdate();
    }

}
