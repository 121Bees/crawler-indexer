/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package DataManager.Indexing.DbModels;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IndexVectorWithTerm {

    String tableSuffix = null;
    String term = null;
    int termid;
    int docid;
    int freq;

    public IndexVectorWithTerm(String tableSuffix, String term, int termid, int docid, int freq) {
        this.tableSuffix = tableSuffix;
        this.term = term;
        this.termid = termid;
        this.docid = docid;
        this.freq = freq;
    }

    public IndexVectorWithTerm(ResultSet rs, String tableSuffix) throws SQLException {
        this.termid = rs.getInt("termid");
        this.docid = rs.getInt("docid");
        this.freq = rs.getInt("freq");
        this.term = rs.getString("term");
        this.tableSuffix = tableSuffix;
    }

    public int getTermId() {
        return termid;
    }

    public int getDocId() {
        return docid;
    }

    public int getFreq() {
        return freq;
    }

    public String getTerm() {
        return term;
    }

    public String getTableSuffix() {
        return tableSuffix;
    }

}
