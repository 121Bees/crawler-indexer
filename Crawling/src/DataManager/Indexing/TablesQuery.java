/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package DataManager.Indexing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TablesQuery {
    Connection conn;
    String name = "";
    PreparedStatement
            insertIndex = null,
            insertParsedDoc = null,
            insertTfIdf = null;

    public TablesQuery(String name, Connection conn) {
        this.name = name;
        this.conn = conn;
    }

    public String getName() {
        return name;
    }


    public PreparedStatement insertIndex(int term, int doc, int freq)
            throws SQLException, ClassNotFoundException {
        if (insertIndex == null) {
            insertIndex = conn.prepareStatement(
                    "INSERT INTO IndexVector_" + name + " VALUES (?,?,?)");
        }
        insertIndex.setInt(1, term);
        insertIndex.setInt(2, doc);
        insertIndex.setInt(3, freq);
        return insertIndex;
    }

    public boolean isDocParsed(int docId)
            throws SQLException, ClassNotFoundException {
        PreparedStatement stmt = conn.prepareStatement(
                "SELECT count(*) AS c FROM ParsedDoc_" + name + " WHERE doc_id = ?");
        stmt.setInt(1, docId);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) return rs.getInt(1) > 0;
        return false;
    }

    public PreparedStatement insertParsedDoc(int docId)
            throws SQLException, ClassNotFoundException {
        if (insertParsedDoc == null)
            insertParsedDoc = conn.prepareStatement(
                    "INSERT INTO ParsedDoc_" + name + "(doc_id) VALUES (?)");
        insertParsedDoc.setInt(1, docId);
        return insertParsedDoc;
    }

    public PreparedStatement populateDocFreq() throws SQLException {
        return conn.prepareStatement(
                "insert into DocFreq_" + name + "(termid,doc_freq)" +
                        "select termid, count(*) from IndexVector_" + name + " group by termid;");
    }

    public boolean isDocFreqPopulated()
            throws SQLException, ClassNotFoundException {
        PreparedStatement stmt = conn.prepareStatement(
                "SELECT count(*) AS c FROM DocFreq_" + name);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) return rs.getInt(1) > 0;
        return false;
    }

    public PreparedStatement computeTfIdx() throws SQLException {
        return conn.prepareCall(
                "select I.termid, I.docid, (1+log(I.freq))*log(42498/D.doc_freq)" +
                        "from DocFreq_" + name + " D, " +
                        "IndexVector_" + name + " I where D.termid=I.termid;");
    }

    public PreparedStatement insertTfIdf(int docId, int termId, double score)
            throws SQLException, ClassNotFoundException {
        if (insertTfIdf == null)
            insertTfIdf = conn.prepareStatement(
                    "INSERT INTO TfIdf_" + name + "(docid, termid, score) " +
                            "VALUES (?,?,?)");
        insertTfIdf.setInt(1, docId);
        insertTfIdf.setInt(2, docId);
        insertTfIdf.setDouble(3, score);
        return insertTfIdf;
    }

    public PreparedStatement getAllDocFreqs()
            throws SQLException {
        return conn.prepareStatement(
                "select * from DocFreq_" + name);
    }

    public PreparedStatement getAllIndexVectors()
            throws SQLException {
        return conn.prepareStatement(
                "select * from IndexVector_" + name);
    }

    public PreparedStatement getAllIndexVectorsWithTerms()
            throws SQLException {
        return conn.prepareStatement(
                "select iv.termid, iv.docid, iv.freq, t.term " +
                        " from IndexVector_" + name + " iv, Terms_" + name + " t" +
                        " where iv.termid = t.id; ");
    }

}
