/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package StrUtil;

public class GenStr {
    public static String stripHttp(String url) {
        String rest = url;
        if (url.startsWith("http://")) rest = url.replace("http://", "");
        else if (url.startsWith("https://")) rest = url.replace("https://", "");
        if (url.trim().endsWith("/")) rest = rest.replace("/", "");
        return rest;
    }

    public static String normalizeURL(String url) {
        if (!url.contains("?")) {
            return url;
        }
        return url.substring(0, url.indexOf("?"));
    }

    public static String getBeforeUrlQuery(String str) {
        int queryPos = str.indexOf('?');
        if (queryPos != -1) return str.substring(0, queryPos);
        return str;
    }

    public static boolean isAcceptableChar(char c) {
        return c >= 48 && c <= 57 // numbers
                || c >= 65 && c <= 90 // capitals
                || c >= 97 && c <= 122; // lower case
    }
}
