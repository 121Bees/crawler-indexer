/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer;

import DataManager.DbManager;
import DataManager.Indexing.TablesBuilder;
import DataManager.Indexing.TablesQuery;
import Indexer.DocumentSource.IDocSource;
import Indexer.DocumentSource.TokenDoc;
import Indexer.IndexerUtil.LinguisticModule;
import Indexer.IndexerUtil.TermDocu;
import Indexer.IndexerUtil.TfIdfAllTerms;
import Indexer.IndexerUtil.TfIdfResult;
import StrUtil.GenStr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class TfIdfIndexer {
    String name = "";
    TablesBuilder tbls;
    TablesQuery query;
    IDocSource src;
    ArrayList<TfIdfResult> tfIdfResult = new ArrayList<>();

    private TfIdfAllTerms allTerms;
    private HashMap<Integer, LinkedList<TermDocu>> term_doc = new HashMap<>();
    private HashMap<String, Integer> freq = new HashMap<>();
    private ArrayList<Integer> docParsed = new ArrayList<>();

    public TfIdfIndexer(TfIdfAllTerms allTerms, String name, IDocSource src)
            throws SQLException, ClassNotFoundException {
        this.allTerms = allTerms;
        this.src = src;
        this.name = name;
        tbls = new TablesBuilder(name, allTerms, DbManager.getInstance().getConn());
        query = tbls.getQuery();
        tbls.constructIndexTables();

    }

    public void initialize()
            throws SQLException, ClassNotFoundException {
        loadParsedDocuments();
        loadTermsFromDb();
        loadVectorIndexFromDb();
    }

    private void loadParsedDocuments() throws SQLException, ClassNotFoundException {
        PreparedStatement stmt = DbManager.getInstance().getConn().prepareStatement(
                "SELECT doc_id FROM ParsedDoc_" + name);
        ResultSet res = stmt.executeQuery();
        while (res.next())
            docParsed.add(res.getInt(1));
    }

    public void tokenize() {
        tokenizeDocuments();
    }

    private void tokenizeDocuments() {
        try {
            ResultSet rsAllUrl = DbManager.getInstance().getPrepStmt_AllUrl().executeQuery();
            rsAllUrl.last();
            rsAllUrl.beforeFirst();
            DbManager.getInstance().getConn().setAutoCommit(false);
            while (rsAllUrl.next()) {
                int docId = rsAllUrl.getInt("id");
                if (!docParsed.contains(docId)) {
                    TokenDoc doc = src.getSource(docId);
                    parseDoc(doc.getId(), doc.getContent(), doc.getUrl());
                    updateVectorIndex();
                    query.insertParsedDoc(docId).executeUpdate();
                    DbManager.getInstance().getConn().commit();
                } else System.out.printf("[%s] skipped\n", docId);
            }
            DbManager.getInstance().getConn().setAutoCommit(true);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadTermsFromDb()
            throws SQLException, ClassNotFoundException {
        PreparedStatement s = DbManager.getInstance().getConn().prepareStatement("select * from Terms_" + allTerms.getName());
        ResultSet rs = s.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String term = rs.getString("term");
            if (!allTerms.getTerms().containsKey(term))
                allTerms.getTerms().put(term, id);
        }
    }

    private void loadVectorIndexFromDb()
            throws SQLException, ClassNotFoundException {
        PreparedStatement s = DbManager.getInstance().getConn().prepareStatement("select * from IndexVector_" + name);
        ResultSet rs = s.executeQuery();
        while (rs.next()) {
            int id = rs.getInt(1);
            if (!term_doc.containsKey(id))
                term_doc.put(id, new LinkedList<TermDocu>());
            LinkedList<TermDocu> termDoc = term_doc.get(id);
            termDoc.add(new TermDocu(rs.getInt("docid"), rs.getInt("freq")));
        }
    }

    private void updateVectorIndex()
            throws SQLException, ClassNotFoundException {
        Iterator itr = term_doc.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Integer, LinkedList<TermDocu>> entry = (Map.Entry) itr.next();
            int term = entry.getKey();
            LinkedList<TermDocu> termValue = entry.getValue();
            for (TermDocu tDoc : termValue)
                query.insertIndex(term, tDoc.getDocId(),
                        tDoc.getFrequency()).executeUpdate();
            termValue.clear();
        }
    }

    private void parseDoc(int doc, String text, String url) throws SQLException {
        int parsedLen = parse(doc, text);
        System.out.printf("[%s] %s : %d\n", doc, url, parsedLen);
    }

    private int parse(int docid, String text) {
        freq.clear();
        String token = "";
        char c;
        int parsed = 0;
        boolean alph = false;
        for (int i = 0; i < text.length(); i++) {
            c = text.charAt(i);
            if (GenStr.isAcceptableChar(c)) {
                token += c;
            } else if ((c == ' ' || c == '\t' || c == '\n' || c == '\r')
                    && token.trim().length() > 0) {
                if (!token.isEmpty()
                        && !LinguisticModule.isStopWord(token)
                        && !LinguisticModule.repeatingCharacter(token)
                        && token.length() > 1) {
                    String term = LinguisticModule.process(token.toLowerCase());
                    addNewTerm(term);
                    if (freq.containsKey(term))
                        freq.put(term, freq.get(term) + 1);
                    else freq.put(term, 1);
                    parsed++;
                }
                token = "";
            } else token = "";
        }

        if (token.length() > 0
                && !LinguisticModule.isStopWord(token)) {
            String term = LinguisticModule.process(token);
            addNewTerm(term);
            if (freq.containsKey(term)) {
                freq.put(term, freq.get(term) + 1);
            } else freq.put(term, 1);
        }

        updatePosting(docid);
        return parsed;
    }

    private void addNewTerm(String term) {
        if (!allTerms.getTerms().containsKey(term)) {
            int id = allTerms.getTerms().size();
            allTerms.getTerms().put(term, id);
            try {
                allTerms.insertTermToMySql(term, id).executeUpdate();
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }

        int id = allTerms.getTerms().get(term);
        if (!term_doc.containsKey(id)) {
            term_doc.put(id, new LinkedList<>());
        }

    }
    /*private String[] separateNumLetters(String term){
        for(int i=0;i<term.length();i++){

        }

    }*/


    private void updatePosting(int doc) {
        for (String term : freq.keySet()) {
            if (!term_doc.containsKey(allTerms.getTerms().get(term)))
                term_doc.put(allTerms.getTerms().get(term), new LinkedList<TermDocu>());
            TermDocu termDoc = new TermDocu(doc, freq.get(term));
            term_doc.get(allTerms.getTerms().get(term)).add(termDoc);
        }
    }

    public String getName() {
        return name;
    }
}
