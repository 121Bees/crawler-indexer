/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.LinearCombination.DbActions;

import DataManager.Indexing.TableFinal;
import Indexer.IndexerUtil.TermDocument;
import Indexer.LinearCombination.NewTfIdfAction;

import java.sql.SQLException;

public class StoreAsNew extends NewTfIdfAction {
    @Override
    public void Triggered(TermDocument tDoc, double w) {
        try {
            TableFinal.insertTfIdf(tDoc, w)
                    .executeUpdate();
            System.out.printf("[%s] persisted (new)\n", tDoc.getTerm());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
