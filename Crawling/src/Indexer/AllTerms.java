/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer;

import Indexer.IndexerUtil.TermDocument;
import Indexer.IndexerUtil.TfIdfResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class AllTerms {
    HashMap<String, ArrayList<TfIdfResult>> allTerms = new HashMap<>();

    public void add(TfIdfResult[] bodyResults) {
        for (TfIdfResult res : bodyResults) {
            String key = res.getIndexVector().getTerm();
            if (!allTerms.containsKey(key)) {
                ArrayList<TfIdfResult> lstTfIdf = new ArrayList<>();
                lstTfIdf.add(res);
                allTerms.put(key, lstTfIdf);
            } else allTerms.get(key).add(res);
        }
    }

    public HashMap<String, ArrayList<TfIdfResult>> getAllTerms() {
        return allTerms;
    }

    public Set<String> getKeys() {
        return allTerms.keySet();
    }

    public ArrayList<TfIdfResult> get(String key) {
        return allTerms.get(key);
    }

    public void remove(String key) {
        allTerms.remove(key);
    }

    public void remove(TermDocument tDoc) {
        ArrayList<TfIdfResult> res = allTerms.get(tDoc.getTerm());
        if (res != null) {
            System.out.printf("[%s : %d] Loaded\n", tDoc.getTerm(), tDoc.getDocId());
            ArrayList<TfIdfResult> cand = new ArrayList<>();
            for (TfIdfResult r : res)
                if (r.getIndexVector().getDocId() == tDoc.getDocId()
                        && r.getIndexVector().getTableSuffix().equals(tDoc.getSuffix())
                        && r.getIndexVector().getTermId() == tDoc.getTermid())
                    cand.add(r);
            for (TfIdfResult c : cand) res.remove(c);
            if (res.size() == 0) {
                allTerms.remove(tDoc.getTerm());
                System.out.printf("[%s] Term has been removed.\n", tDoc.getTerm());
            }
        } else System.out.printf("[%s] does not exist\n", tDoc.getTerm());
    }
}
