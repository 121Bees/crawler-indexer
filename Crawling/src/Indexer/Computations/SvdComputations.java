/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.Computations;

import DataManager.DbManager;
import org.jblas.DoubleMatrix;
import org.jblas.Singular;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SvdComputations {
    static DoubleMatrix dm;
    static int CIdBase = 450;
    static int conceptsnum = 50;//has to be smaller than samplesize
    static int docs = 0, terms = 0, sampleSize = 200, sampleNum = 4;
    static int docoffset = 3564;
    static DbManager db;

    public static void main(String[] args)
            throws ClassNotFoundException {
        db = DbManager.promptCredentialsByConsole();
        initMatrix();
        populateMatrix();

        Singular sing = new Singular();
        DoubleMatrix[] res = sing.sparseSVD(dm);

        DoubleMatrix M = inverse(res[1]).mmul(res[0].transpose());
        /*insert M into the database
        each row in M is corresponding to one concept we find in the sample
        need to store the <concept id, termid, weight> triple into db
        */
        try {
            db.getConn().setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < conceptsnum; i++) {
            for (int j = 0; j < M.columns; j++) {
                if (M.get(i, j) > 0.001) {
                    try {
                        db.InsertIntoM(CIdBase + i, j, M.get(i, j));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                db.getConn().commit();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    private static DoubleMatrix inverse(DoubleMatrix re) {
        DoubleMatrix inverse = DoubleMatrix.zeros(sampleSize, sampleSize);
        for (int i = 0; i < conceptsnum; i++) {
            inverse.put(i, i, 1 / re.get(i, 0));
        }
        return inverse;
    }

    private static void populateMatrix() {
        //get a random sample and selecting only the docids in the sample from Tfidf(termid,docid,score)
        /*
        step 1: generate 1000 random numbers in [142,498]
        step 2: retrieve the indexvector of these document sequentially and put them in the next column of dm
         */

        try {
            takeSample();
            ResultSet rs = db.getConn().prepareStatement(
                    "select T.termid,T.docid,T.score from " +
                            "TfIdf_TrimmedComplete T,Sample S " +
                            "where T.docid=S.docid order by T.docid;").executeQuery();
            int column = -1;
            int prev = -1;
            while (rs.next()) {
                if (rs.getInt(2) != prev) {
                    column++;
                    prev = rs.getInt(2);
                }
                dm.put(rs.getInt(1), column, rs.getDouble(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void takeSample() {
        try {
            db.getConn().prepareStatement("drop table if exists Sample").executeUpdate();
            db.getConn().prepareStatement("create table if not exists Sample(docid int);").executeUpdate();
            db.getConn().prepareStatement("Insert into Sample (" +
                    "select id from Website " +
                    "order by rand() limit " + sampleSize +
                    ");").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void initMatrix() {
        try {
            ResultSet rs = db.getConn().prepareStatement("select count(*) from Website").executeQuery();
            rs.next();
            docs = rs.getInt(1);
            rs = db.getConn().prepareStatement("select count(*) from Terms_Trimmed ").executeQuery();
            rs.next();
            terms = rs.getInt(1);
            dm = DoubleMatrix.zeros(terms, sampleSize);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
