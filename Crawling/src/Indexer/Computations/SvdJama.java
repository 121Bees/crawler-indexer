/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.Computations;

import DataManager.DbManager;
import Jama.Matrix;
import Jama.SingularValueDecomposition;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SvdJama {
    static float[][] mat;
    private static DbManager db;

    public static void main(String[] args)
            throws ClassNotFoundException {
        db = DbManager.promptCredentialsByConsole();
        //mat = initMatrix();
        //populateMatrix(mat);
        Matrix mtr = new Matrix(40000, 6000, 0);
        SingularValueDecomposition svd = new SingularValueDecomposition(mtr);
        Matrix svdU = svd.getU();
        Matrix svdS = svd.getS();
        Matrix svdV = svd.getV();
    }

    private static void populateMatrix(float[][] m) {
        try {
            ResultSet rs = db.getConn().prepareStatement("select * from AllTfIdf_Normalized").executeQuery();
            while (rs.next()) {
                m[rs.getInt(1)][rs.getInt(2)] = rs.getFloat(3);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static float[][] initMatrix() {
        try {
            //todo: refactor this
            ResultSet rs = db.getConn().prepareStatement("select count(*) from Website").executeQuery();
            rs.next();
            int docs = rs.getInt(1);
            rs = db.getConn().prepareStatement("select count(*) from Terms_Trimmed ").executeQuery();
            rs.next();
            int terms = rs.getInt(1);
            return mat = createZeroMatrix(terms, docs);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static float[][] createZeroMatrix(int m, int n) {
        // 42498
        // 151683
        float[][] matrix = new float[42498][5700];
        for (int x = 0; x < m; ++x)
            for (int y = 0; y < n; ++y)
                matrix[x][y] = 0.0f;
        return matrix;
    }
}
