/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.IndexerUtil;

import DataManager.DbManager;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

public class TfIdfAllTerms {
    PreparedStatement insertTerm = null;
    HashMap<String, Integer> terms = new HashMap<>();
    String name;
    DbManager db;

    public TfIdfAllTerms(String name)
            throws SQLException, ClassNotFoundException {
        this.name = name;
        db = DbManager.getInstance();
        constructTermsTable();
    }

    public HashMap<String, Integer> getTerms() {
        return terms;
    }

    private void constructTermsTable() throws SQLException, ClassNotFoundException {
        db.getConn().prepareStatement(
                "create table if not exists 121Crawler.Terms_"
                        + name
                        + "(id Integer, term text, primary key(id));")
                .executeUpdate();
    }

    public void dropIndexTable()
            throws SQLException, ClassNotFoundException {
        db.getConn().prepareStatement(
                "DROP TABLE IF EXISTS 121Crawler.Terms_" + name + ";")
                .executeUpdate();
    }

    public String getName() {
        return name;
    }

    public PreparedStatement insertTermToMySql(String term, int ID)
            throws SQLException, ClassNotFoundException {
        if (insertTerm == null) {
            insertTerm = db.getConn().prepareStatement(
                    "insert into Terms_" + name + "(id,term) values (?,?)");
        }
        insertTerm.setString(2, term);
        insertTerm.setInt(1, ID);
        return insertTerm;
    }
}
