/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.IndexerUtil;

public class TermDocu {
    int docId;
    int frequency;

    public TermDocu(int docId, int frequency) {
        this.docId = docId;
        this.frequency = frequency;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TermDocu) {
            TermDocu tDoc = (TermDocu) obj;
            return tDoc.getDocId() == this.docId
                    && tDoc.getFrequency() == this.frequency;
        }
        return false;
    }
}
