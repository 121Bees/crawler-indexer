/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.IndexerUtil;

public class TermDocument {
    int docId;
    String term;
    String suffix = "";
    int termid = -1;

    public TermDocument(int docId, String term, String suffix, int termid) {
        this.docId = docId;
        this.term = term;
        this.suffix = suffix;
        this.termid = termid;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public int getTermid() {
        return termid;
    }

    public void setTermid(int termid) {
        this.termid = termid;
    }

/*    public TermDocument(int docId, String term) {
        this.docId = docId;
        this.term = term;
    }*/

    public String getTerm() {
        return term;
    }

    public int getDocId() {
        return docId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TermDocument) {
            TermDocument tDoc = (TermDocument) obj;
            return tDoc.getDocId() == this.docId
                    && tDoc.getTerm().equals(this.term)
                    && tDoc.getSuffix().equals(this.suffix)
                    && tDoc.getTermid() == this.termid;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
