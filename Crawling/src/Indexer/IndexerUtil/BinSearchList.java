/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.IndexerUtil;

import java.util.*;

public class BinSearchList implements List<String> {
    static int cachedMid = -1;
    int dupelicates = 0;
    ArrayList<String> items = new ArrayList<>();

    public static int binarySearch(String key, ArrayList<String> a) {
        int lo = 0;
        int hi = a.size() - 1;
        while (lo <= hi) {
            int mid = cachedMid = lo + (hi - lo) / 2;
            if (key.compareTo(a.get(mid)) < 0) hi = mid - 1;
            else if (key.compareTo(a.get(mid)) > 0) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    public static int binarySearchIgnoreCase(String key, ArrayList<String> a) {
        int lo = 0;
        int hi = a.size() - 1;
        while (lo <= hi) {
            int mid = cachedMid = lo + (hi - lo) / 2;
            if (key.compareToIgnoreCase(a.get(mid)) < 0) hi = mid - 1;
            else if (key.compareToIgnoreCase(a.get(mid)) > 0) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof String)
            return binarySearch((String) o, items) == -1;
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof String) {
            int pos = binarySearch((String) o, items);
            if (pos != -1) {
                items.remove(pos);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(String s) {
        if (items.size() < 1) items.add(s);
        int pos = binarySearch(s, items);
        if (pos == -1) {
            int insPos = cachedMid;
            if (cachedMid > items.size()) insPos = items.size() - 1;
            items.add(insPos, s);
            return true;
        }
        return false;
    }

    @Override
    public int indexOf(Object o) {
        return binarySearch((String) o, items);
    }

    public int indexOfIgnoreCase(Object o) {
        return binarySearchIgnoreCase((String) o, items);
    }

    @Override
    public int lastIndexOf(Object o) {
        return items.lastIndexOf(o);
    }


    @Override
    public void add(int i, String s) {
        items.add(i, s);
    }

    @Override
    public String set(int i, String s) {
        return items.set(i, s);
    }

    @Override
    public Iterator<String> iterator() {
        return items.iterator();
    }

    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return items.toArray(ts);
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return items.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends String> collection) {
        return items.addAll(collection);
    }

    @Override
    public boolean addAll(int i, Collection<? extends String> collection) {
        return items.addAll(i, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Override
    public String get(int i) {
        return items.get(i);
    }

    @Override
    public String remove(int i) {
        return items.remove(i);
    }

    @Override
    public ListIterator<String> listIterator() {
        return items.listIterator();
    }

    @Override
    public ListIterator<String> listIterator(int i) {
        return items.listIterator(i);
    }

    @Override
    public List<String> subList(int i, int i1) {
        return items.subList(i, i1);
    }

}
