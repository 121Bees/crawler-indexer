/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.TokenizationStrategies;

import Indexer.DocumentSource.TokenDoc;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TitleSource extends HtmlSource {
    @Override
    public TokenDoc getSource(int id)
            throws SQLException, ClassNotFoundException {
        ResultSet rs = getHtml(id);
        while (rs.next()) {
            String html = rs.getString(2);
            return new TokenDoc(rs.getInt(1), parseTitle(html), rs.getString(3));
        }
        return null;
    }

    private String parseTitle(String html) {
        String lower = html.toLowerCase();
        int begin = lower.indexOf("<title>");
        int end = lower.indexOf("</title>");
        if (begin > -1 && end > -1)
            return html.substring(begin + "<title>".length(), end);
        return "";
    }
}
