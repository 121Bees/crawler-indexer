/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Indexer.TokenizationStrategies;

import Indexer.DocumentSource.TokenDoc;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HeadSource extends HtmlSource {
    @Override
    public TokenDoc getSource(int id)
            throws SQLException, ClassNotFoundException {
        ResultSet rs = getHtml(id);
        while (rs.next()) {
            String html = rs.getString(2);
            return new TokenDoc(rs.getInt(1), parseHead(html), rs.getString(3));
        }
        return null;
    }

    private String parseHead(String html) {
        Document doc = Jsoup.parse(html);
        Elements elms = doc.select("meta");
        String result = "";
        for (Element elm : elms) {
            if (elm.attr("name").toLowerCase().contains("description"))
                result += (elm.attr("content") + " ");
            if (elm.attr("name").toLowerCase().contains("keyword"))
                result += (elm.attr("content") + " ");
            if (elm.attr("name").toLowerCase().contains("author"))
                result += (elm.attr("content") + " ");
        }
        return result;
    }
}
