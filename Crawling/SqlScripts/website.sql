CREATE TABLE IF NOT EXISTS `beesse`.`Website` (
  `Id`             INT           NOT NULL AUTO_INCREMENT,
  `Url` VARCHAR(2048) NOT NULL,
  `Noramlized_Url` VARCHAR(2048) NOT NULL,
  `Html_Content` MEDIUMTEXT NULL,
  `Text_Content` MEDIUMTEXT NULL,
  `Outgoing_Links` MEDIUMTEXT NULL,
  PRIMARY KEY (`Id`));

