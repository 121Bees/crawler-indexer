CREATE TABLE IF NOT EXISTS beesse.Terms (
  id   INTEGER,
  term TEXT,
  PRIMARY KEY (id)
);